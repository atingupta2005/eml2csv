import datetime
import json
import eml_parser
import csv


import os

folderName = "C:/Users/Atin/Documents/Thunderbird/Starred_20190616-1549/messages/"
csv_columns = ["FileName", "Subject", "From", "To", "Date", "CC"]

with open(folderName + "Output.csv", "w", encoding="utf-8", newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
    writer.writeheader()

    for filename in os.listdir(folderName):
        strFilePath_Name = folderName + filename

        with open(strFilePath_Name, 'rb') as fhdl:
            raw_email = fhdl.read()


        parsed_eml = eml_parser.eml_parser.decode_email_b(raw_email)

        strSubject = parsed_eml['header'].get('subject',"")
        strFrom = parsed_eml['header'].get('from',"")
        strTo = parsed_eml['header'].get('to',"")
        strDate = parsed_eml['header'].get('date',"")
        strCC = parsed_eml['header'].get('cc',"")

        strDate = strDate.strftime('%m-%d-%Y')
        strTo = ";".join(strTo)
        strCC = ";".join(strCC)

        strRow = {'FileName': filename, 'Subject': strSubject, 'From': strFrom, "To": strTo, "CC": strCC, "Date": strDate}

        writer.writerow(strRow)

